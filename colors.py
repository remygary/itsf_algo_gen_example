LIGHT_RED = '\033[91m'
LIGHT_BLUE = '\033[94m'
LIGHT_GREEN = '\033[92m'
LIGHT_CYAN = '\033[96m'
DEFAULT = '\033[0m'

UP_ONE_LINE = '\033[1A'
UP_TWO_LINE = '\033[2A'
ERASE_LINE = '\033[K'

from colors import *

def print_current_results(generation, iteration, total, prefix, colored_individual, genetic_method):
    decimals = 1
    length = 100
    fill = '█'
    percent = 100 * (iteration / float(total))
    print('\r%sGeneration: %s | Best result: %.1f%% | population_size = %d | best_sample = %d | lucky_few = %d chance_of_mutation = %d | %s %s' % (DEFAULT, generation, percent,
                                                                                                                                                   genetic_method.population_size, genetic_method.best_sample, genetic_method.lucky_few, genetic_method.chance_of_mutation,
                                                                                                                                                   colored_individual, DEFAULT), end='\r')
    if iteration == total:
        print()

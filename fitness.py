def get_fitness(guess:[], password:[]) -> int :
    length = len(password)
    res = 0
    for i in range(length):
        if guess[i] == password[i]:
            res += 1
    return res * 100 / length

import random
from colors import *

def gen_password(length: int) -> str:
    i = 0
    res = ""
    while i < length:
        letter = chr(97 + int(26 * random.random()))
        res += letter
        i += 1
    return res


def print_colored_genome(genome: str, to_find_password: str):
    i = 0
    while i < len(genome):
        if genome[i] == to_find_password[i]:
            print(LIGHT_GREEN + genome[i], end='')
        else:
            print(LIGHT_RED + genome[i], end='')
        i += 1
    print(DEFAULT)


def format_colored_genome(genome: str, to_find_password: str) -> str:
    i = 0
    result = ""
    while i < len(genome):
        if genome[i] == to_find_password[i]:
            result += LIGHT_GREEN + genome[i]
        else:
            result += LIGHT_RED + genome[i]
        i += 1
    return result


def get_random_pos_neg():
    return 1 if random.random() < 0.5 else -1



import argparse
import time

from genetic_method import GeneticMethodFoundException
from genetic_for_genetic_method import *

from utilities import *
from global_var import *


def single_genetic_method(population_size, best_sample, lucky_few, chance_of_mutation):
    print('Start !')
    start = time.time()
    to_find_password = gen_password(password_length)
    genetic_method = GeneticMethod(population_size, gen_password(password_length), best_sample, lucky_few, chance_of_mutation)
    try:
        genetic_method.run()
    except GeneticMethodFoundException as e:
        print(e, end='')
        print(ERASE_LINE)
        print_colored_genome(to_find_password, to_find_password)
    end = time.time()
    print(f'Done !\nTook: {end - start:0.5f} sec')


def multiple_genetic_method(password_length):
    print('Start !')
    start = time.time()
    to_find_password = gen_password(password_length)
    genetic_for_genetic_method = GeneticForGeneticMethod(POPULATION_SIZE_STEP, to_find_password, BEST_SAMPLE_STEP, LUCKY_FEW_STEP, CHANCE_OF_MUTATION_STEP)
    try:
        genetic_for_genetic_method.run()
    except GeneticForGeneticMethodFoundException as e:
        print(e, end='')
        print(ERASE_LINE)
    end = time.time()
    print(f'Done !\nTook: {end - start:0.5f} sec')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-l', dest='password_length', default=100)
    parser.add_argument('-c', dest='count', default=1000)
    parser.add_argument('-p', dest='population_size', default=50)
    parser.add_argument('-b', dest='best_sample', default=20)
    parser.add_argument('-f', dest='lucky_few', default=20)
    parser.add_argument('-m', dest='chance_of_mutation', default=5)
    args = parser.parse_args()

    password_length     = int(args.password_length)
    count               = int(args.count)
    population_size     = int(args.population_size)
    best_sample         = int(args.best_sample)
    lucky_few           = int(args.lucky_few)
    chance_of_mutation  = int(args.chance_of_mutation)

    # single_genetic_method(population_size, best_sample, lucky_few, chance_of_mutation)

    multiple_genetic_method(password_length)

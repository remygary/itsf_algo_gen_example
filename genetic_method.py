import operator

from utilities import *
from fitness import *
from print_results import *


class GeneticMethodFoundException(Exception):
    def __init___(self, dErrorArguments):
        Exception.__init__(self, "{0}".format(dErrorArguments))
        self.dErrorArguments = dErrorArguments

class GeneticMethodToMuchGenerationException(Exception):
    def __init___(self, dErrorArguments):
        Exception.__init__(self, "{0}".format(dErrorArguments))
        self.dErrorArguments = dErrorArguments



class GeneticMethod:

    def __init__(self, population_size, to_find_password, best_sample, lucky_few, chance_of_mutation) -> None:
        super().__init__()
        self.population_size = population_size
        self.to_find_password = to_find_password
        self.best_sample = best_sample
        self.lucky_few = lucky_few
        self.chance_of_mutation = chance_of_mutation
        self.generation_count = 0

    def generate_first_population(self, population_size):
        population = []
        i = 0
        while i < population_size:
            population.append(gen_password(len(self.to_find_password)))
            i += 1
        return population


    # #################################################
    # SELECTION PROCESS
    # #################################################
    def compute_perf_population(self, population):
        self.generation_count += 1
        population_perf = {}
        for individual in population:
            population_perf[individual] = get_fitness(individual, self.to_find_password)
            if population_perf[individual] == 100:
                # raise FoundException('Found it in ' + str(self.generation_count) + ' generation ! ' + individual + ' = ' + self.to_find_password)
                raise GeneticMethodFoundException('Found it in ' + str(self.generation_count) + ' generation ! ')
        return sorted(population_perf.items(), key=operator.itemgetter(1), reverse=True)

    def select_from_population(self, population_sorted):
        next_generation = []
        for i in range(self.best_sample):
            next_generation.append(population_sorted[i][0])
        for i in range(self.lucky_few):
            next_generation.append(random.choice(population_sorted)[0])
        random.shuffle(next_generation)
        return next_generation

    # #################################################
    # BREEDING PROCESS
    # #################################################
    def create_child(self, individual1, individual2):
        child = ""
        for i in range(len(individual1)):
            if int(100 * random.random()) < 50:
                child += individual1[i]
            else:
                child += individual2[i]
        return child

    def create_children(self, breeders, number_of_child):
        next_population = []
        for i in range(int(len(breeders)/2)):
            for j in range(number_of_child):
                next_population.append(self.create_child(breeders[i], breeders[len(breeders) - 1 - i]))
        return next_population

    # #################################################
    # MUTATION PROCESS
    # #################################################
    def mutate_word(self, word):
        index_modification = int(random.random() * len(word))
        if index_modification == 0:
            word = chr(97 + int(26 * random.random())) + word[1:]
        else:
            word = word[:index_modification] + chr(97 + int(26 * random.random())) + word[index_modification+1:]
        return word

    def mutate_population(self, population):
        for i in range(len(population)):
            if random.random() * 100 < self.chance_of_mutation:
                population[i] = self.mutate_word(population[i])
        return population

    def run(self):
        # GENERATE FIRST RANDOM POPULATION (That's your random base)
        population = self.generate_first_population(self.population_size)
        while True:
            if self.generation_count > 1000:
                raise GeneticMethodToMuchGenerationException('')
            population = self.compute_perf_population(population)
            print_current_results(self.generation_count, population[0][1], 100, '', format_colored_genome(population[0][0], self.to_find_password), self)
            # print(f'Best individual is: {population[0][1]}')
            # print_colored_genome(population[0][0], self.to_find_password)

            # SELECTION.  Bit of the best and bit of random. The best because we want to converge, find a "perfect" solution,
            # obviously, but also some random lucky ones because it prevent reaching only local optimums
            population = self.select_from_population(population)

            # BREEDING. Get your selected population and have them reproduce between them.
            population = self.create_children(population, self.population_size - len(population))

            # MUTATION. Even so often, while reproducing, a mutation impact the DNA of the offspring, making it just a bit
            # different, maybe more, maybe less adapted to the environment.
            population = self.mutate_population(population)

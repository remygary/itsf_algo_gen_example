import operator
import sys

from genetic_method import GeneticMethod, GeneticMethodFoundException, GeneticMethodToMuchGenerationException
from utilities import *
from fitness import *
from print_results import *
from global_var import *

class GeneticForGeneticMethodFoundException(Exception):
    def __init___(self, dErrorArguments):
        Exception.__init__(self, "{0}".format(dErrorArguments))
        self.dErrorArguments = dErrorArguments


class GeneticForGeneticMethod:

    def __init__(self, population_size_step, to_find_password, best_sample_step, lucky_few_step,
                 chance_of_mutation_step) -> None:
        super().__init__()
        self.population_size = GENETICFORGENETICMETHOD_POPULATION_SIZE
        self.population_size_step = population_size_step
        self.to_find_password = to_find_password
        self.best_sample_step = best_sample_step
        self.lucky_few_step = lucky_few_step
        self.chance_of_mutation_step = chance_of_mutation_step
        self.generation_count = 0

    def generate_first_population(self, population_size):
        population = []
        i = 0
        while i < population_size:
            pop_size = int(random.random() * MAX_POPULATION_SIZE)
            pop_size = MIN_POPULATION_SIZE if pop_size < MIN_POPULATION_SIZE else pop_size

            selected_sample = int(random.random() * pop_size)
            selected_sample = MIN_SELECTED_SAMPLE if selected_sample < MIN_SELECTED_SAMPLE else selected_sample

            lucky_few = int(random.random() * (pop_size - selected_sample))
            lucky_few = MIN_LUCKY_FEW if lucky_few < MIN_LUCKY_FEW else lucky_few

            mutation_rate = int(random.random() * 100)
            mutation_rate = MIN_MUTATION_RATE if mutation_rate < MIN_MUTATION_RATE else mutation_rate

            population.append(GeneticMethod(pop_size, self.to_find_password, selected_sample, lucky_few, mutation_rate))
            i += 1
        return population

    #     print('\r%sGeneration: %s | Best result: %.1f%% | %s %s' % (DEFAULT, generation, percent, colored_individual, DEFAULT), end='\r')
    # #################################################
    # SELECTION PROCESS
    # #################################################
    def compute_perf_population(self, population: [GeneticMethod]):
        self.generation_count += 1
        population_perf = {}
        best_perf = -10000000000
        for individual in population:
            try:
                individual.run()
            except GeneticMethodFoundException as e:
                population_perf[individual] = -individual.generation_count
            except IndexError as e:
                population_perf[individual] = -10000000000
            except GeneticMethodToMuchGenerationException as e:
                population_perf[individual] = -10000000000

            print('\r%sIndividual performance is: %d population_size = %d | best_sample = %d | lucky_few = %d chance_of_mutation = %d' % (ERASE_LINE, population_perf[individual], individual.population_size, individual.best_sample, individual.lucky_few, individual.chance_of_mutation))

            if population_perf[individual] > GENETICFORGENETICMETHOD_TARGET_MAX_GEN:
                print('Found in %d generation with values: population_size = %d | best_sample = %d | lucky_few = %d chance_of_mutation = %d' % (self.generation_count, individual.population_size, individual.best_sample, individual.lucky_few, individual.chance_of_mutation))
                raise GeneticForGeneticMethodFoundException('Found it in ' + str(self.generation_count) + ' generation !')
            best_perf = population_perf[individual] if population_perf[individual] > best_perf else best_perf
        print(LIGHT_RED+'Best took ' + str(best_perf) + ' generations'+DEFAULT)
        return sorted(population_perf.items(), key=operator.itemgetter(1), reverse=True)

    def select_from_population(self, population_sorted: []):
        next_generation = []
        for i in range(GENETICFORGENETICMETHOD_SELECTED_SAMPLE):
            next_generation.append(population_sorted[i][0])
        for i in range(GENETICFORGENETICMETHOD_LUCKY_FEW):
            next_generation.append(random.choice(population_sorted)[0])
        random.shuffle(next_generation)
        return next_generation

    # #################################################
    # BREEDING PROCESS
    # #################################################
    def create_child(self, individual1: GeneticMethod, individual2: GeneticMethod) -> GeneticMethod:

        population_size         = individual1.population_size       if int(100 * random.random()) < 50 else individual2.population_size
        best_sample             = individual1.best_sample           if int(100 * random.random()) < 50 else individual2.best_sample
        lucky_few               = individual1.lucky_few             if int(100 * random.random()) < 50 else individual2.lucky_few
        chance_of_mutation      = individual1.chance_of_mutation    if int(100 * random.random()) < 50 else individual2.chance_of_mutation

        child = GeneticMethod(population_size, self.to_find_password, best_sample, lucky_few, chance_of_mutation)

        return child

    def create_children(self, breeders: [GeneticMethod], number_of_child: int) -> [GeneticMethod]:
        next_population = []
        for i in range(int(len(breeders) / 2)):
            for j in range(number_of_child):
                next_population.append(self.create_child(breeders[i], breeders[len(breeders) - 1 - i]))
        return next_population

    # #################################################
    # MUTATION PROCESS
    # #################################################
    def mutate_child(self, child: GeneticMethod) -> GeneticMethod:
        child.population_size       = child.population_size + int(get_random_pos_neg() * random.random() * self.population_size_step) if int(100 * random.random()) < 50 else child.population_size
        child.best_sample           = child.best_sample + int(get_random_pos_neg() * random.random() * self.best_sample_step) if int(100 * random.random()) < 50 else child.best_sample
        child.lucky_few             = child.lucky_few + int(get_random_pos_neg() * random.random() * self.lucky_few_step) if int(100 * random.random()) < 50 else child.lucky_few
        child.chance_of_mutation    = child.chance_of_mutation + int(get_random_pos_neg() * random.random() * self.chance_of_mutation_step) if int(100 * random.random()) < 50 else child.chance_of_mutation
        return child

    def mutate_population(self, population: [GeneticMethod]) -> [GeneticMethod]:
        for i in range(len(population)):
            if random.random() * 100 < GENETICFORGENETICMETHOD_MUTATION_RATE:
                population[i] = self.mutate_child(population[i])
        return population

    def run(self):
        # GENERATE FIRST RANDOM POPULATION (That's your random base)
        population = self.generate_first_population(self.population_size)
        while True:
            population = self.compute_perf_population(population)

            # SELECTION.  Bit of the best and bit of random. The best because we want to converge, find a "perfect" solution,
            # obviously, but also some random lucky ones because it prevent reaching only local optimums
            population = self.select_from_population(population)

            # BREEDING. Get your selected population and have them reproduce between them.
            population = self.create_children(population, self.population_size - len(population))

            # MUTATION. Even so often, while reproducing, a mutation impact the DNA of the offspring, making it just a bit
            # different, maybe more, maybe less adapted to the environment.
            population = self.mutate_population(population)
